#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

#define HEAP 1024
#define MALLOC 256
#define FIRST 128
#define SECOND 256
#define THIRD 512
#define FOURTH 1000
#define MAX 2048


void init() {
	heap_init(HEAP);	
}

void test1() {
    printf("----TEST 1-----\n");
    void *mem = _malloc(MALLOC);
    debug_heap(stdout, HEAP_START);
    _free(mem);
    printf("----TEST 1 PASSED-----\n");
}

void test2() {
    printf("----TEST 2-----\n");
    void *mem1 = _malloc(FIRST);
    void *mem2 = _malloc(SECOND);
    void *mem3 = _malloc(THIRD);
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    _free(mem3);
    printf("----TEST 2 PASSED-----\n");
}

void test3() {
    printf("----TEST 3-----\n");
    void *mem1 = _malloc(SECOND);
    void *mem2 = _malloc(THIRD);
    void *mem3 = _malloc(FOURTH);
    _free(mem2);
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem3);
    printf("----TEST 3 PASSED-----\n");
}

void test4() {
    printf("----TEST 4-----\n");
    void *mem = _malloc(MAX);
    debug_heap(stdout, HEAP_START);
    _free(mem);
    printf("----TEST 4 PASSED-----\n");
}

void test5() {
    printf("----TEST 5-----\n");
    void *mem1 = _malloc(MAX);
    void *mem2 = _malloc(MAX);
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    printf("----TEST 5 PASSED-----\n");    
}

int main(){
    init();
    test1();
    test2();
    test3();
    test4();
    test5();
    return 0;
}
